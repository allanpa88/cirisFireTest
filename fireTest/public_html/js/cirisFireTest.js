/** 
  * @desc This file contains the different functions requested in the fire test
  * to apply for the Javascript position in CIRIS Informatic Solutions
  * 
  * @author Allan Porras allanpa88@gmail.com
*/

/**
  * @desc Check whether a given variable is a Number or not
  * 
  * @param n the variable to check
  * @return true if the variable is a Number, false otherwise
 */ 
 function isNumber( n ) {
     return n.constructor === Number;     
 }

/**
  * @desc Sum all the numbers in a given list and return its value with the 
  * use of for loop
  * 
  * @param list - the list of numbers to perform the sum
  * @return  - result with the the sum of all numbers in the list, 
  * return 0 if the list is empty
*/
function forSumOfListElements( list ) {
    var result = 0;
    if( Array.isArray( list ) && list.every( isNumber ) ) {
        for ( var listIndex = 0; listIndex < list.length; listIndex++ ) {
            result += list[listIndex];
        }
    }
    return result;
}

/**
  * @desc Sum all the numbers in a given list and return its value with the 
  * use of while loop
  * 
  * @param list - the list of numbers to perform the sum
  * @return  result - with the the sum of all numbers in the list,
  * return 0 if the list is empty
*/
function whileSumOfListElements( list ) {
    var result = 0, listIndex = 0;
    if( Array.isArray( list ) && list.every( isNumber ) ) {
        while ( listIndex < list.length ) {
            result += list[listIndex];
            listIndex++;
        }
    }
    return result;
}

/*
 * @des Sum all the numbers in a given list and return its value with the 
 * use a recursive method with simple Tail Call Optimization.
 * 
 * The recursion is rewritten in such a way that the engine would recognise 
 * a recursive execution was happening and optimise the code 
 * internally into a loop form.
 * 
 * @param list - the list of numbers to perform the sum
 * @return  result - with the the sum of all numbers in the list,
 * return 0 if the list is empty
 */
function tailRecursiveSumOfListElementsOneParameter (list) {
    function  recur( l ) {
        if (l.length === 0 ) {
            return 0;
        } else if (l.length === 1) {
            return l[0];
        }
        else {
            return l.pop() + recur( l );
        }
    }
    return recur (list);
}

/**
  * @desc Combine two list given alternating each element, as the problem 
  * did not specify if list must have the same lengths, this function also 
  * works for list with different sizes.
  * 
  * @param list1 - it's the first list to start alternating elements according the example given
  * @param list2 - it's the second list to alternating elements 
  * @return result - new list with the combination of the lists by parameter
*/
function combineListsAlternatingElements( list1, list2 ) {
    var result = [];
    var minListLength = ( list1.length < list2.length ) ? list1.length : list2.length;
    
    for ( var listIndex = 0; listIndex < minListLength; listIndex++ ) {
        result.push( list1[listIndex], list2[listIndex] );
    }
    
    /*
     *    FIXME: This section validates if one of the arrays is bigger than 
     *     other, and if so then concatenates the difference in the result.
     *     We can just remove it if is not necesary.
    */
    if ( list1.length < list2.length ) {
        result.push( list2.slice( minListLength, list2.length ) );
    } else {
        result.push( list1.slice( minListLength, list1.length ) );
    }
    
    return result;
}